export const environment = {
  production: true,
  apiUrl: 'https://yarn-dragon-api.herokuapp.com/api'
};
