export interface ICart {
  id: number,
  name: string,
  quantity: number,
  stock: number,
  price: number,
  imageUrl: string
}
