export interface IProductItem {
  id: number,
  name: string,
  stock: number,
  price: number,
  shortDesc: string,
  description: string,
  imageUrl: string
}
