import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'yarn-dragon-ng';

  constructor(private router: Router, private snackbar: MatSnackBar, private authService: AuthService) {}

  headerClicked() {
    this.router.navigate(['products']);
  }

  navigateToCart() {
    this.router.navigate(['cart']);
  }

  navigateToLogin() {
    this.router.navigate(['login']);
  }

  logout() {
    localStorage.removeItem('jwt');
    this.router.navigate(['products']);
    this.snackbar.open('You have been logged out', 'Ok', { verticalPosition: 'top'});
  }

  isLoggedIn() {
    return this.authService.jwt && this.authService.jwt.length > 0;
  }
}
