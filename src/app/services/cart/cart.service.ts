import { Injectable } from '@angular/core';
import { IProductItem } from 'src/app/interfaces/product-item';
import { ICart } from '../../interfaces/cart';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  baseUrl = environment.apiUrl;
  private cart: ICart[] = [];

  constructor(private http: HttpClient, private authService: AuthService) {
    this.getCartFromLocalStorage();
  }

  getCart() {
    return this.cart;
  }

  getCartFromLocalStorage() {
    this.cart = JSON.parse(localStorage.getItem('cart'));
  }

  addToCart(item: IProductItem) {
    if (this.cart == null) { this.cart = []; }
    const newCartItem: ICart = {
      id: item.id,
      name: item.name,
      price: item.price,
      quantity: 1,
      stock: item.stock,
      imageUrl: item.imageUrl
    };

    this.cart.push(newCartItem);
    localStorage.setItem('cart', JSON.stringify(this.cart));
  }

  deleteFromCart(cartItem: ICart) {
    const index = this.cart.indexOf(cartItem);
    this.cart.splice(index, 1);
    localStorage.setItem('cart', JSON.stringify(this.cart));
  }

  updateCartQuantity(cartItem: ICart, newQuantity: number) {
    cartItem.quantity = newQuantity;
    localStorage.setItem('cart', JSON.stringify(this.cart));
  }

  clearCart() {
    this.cart = [];
    localStorage.setItem('cart', JSON.stringify(this.cart));
  }

  checkout() {
    const jwtAuthHeader = 'Bearer ' + this.authService.jwt;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': jwtAuthHeader
        });
    const options = { headers: headers };

    const url = `${this.baseUrl}/products/checkout`;
    const body = [];
    this.cart.forEach(item => {
      body.push({
        Id: item.id,
        Stock: item.quantity
      });
    });
    return this.http.put(url, body, options);
  }
}
