import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IProductItem } from '../../interfaces/product-item';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  baseUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) {}

  getAllProducts(): Observable<IProductItem[]> {
    const url = `${this.baseUrl}/products`;
    return this.http.get<IProductItem[]>(url);
  }
}
