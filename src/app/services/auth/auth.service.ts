import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ILoginRequest } from 'src/app/interfaces/login-request';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.apiUrl;
  private _jwt = '';

  constructor(private http: HttpClient) { }

  login(loginRequest: ILoginRequest) {
    const url = `${this.baseUrl}/auth/login`;
    return this.http.post(url, loginRequest);
  }

  register(registerRequest: ILoginRequest) {
    const url = `${this.baseUrl}/auth/register`;
    return this.http.post(url, registerRequest);
  }

  set jwt(newJwt) {
    localStorage.setItem('jwt', newJwt);
    this._jwt = newJwt;
  }

  get jwt() {
    if (this._jwt && this._jwt !== '') {
      return this._jwt;
    }
    this._jwt = localStorage.getItem('jwt');
    return this._jwt;
  }

  isAuthenticated(): boolean {
    const jwtHelperService = new JwtHelperService();
    const token = this.jwt;
    return token && !jwtHelperService.isTokenExpired(token);
  }
}
