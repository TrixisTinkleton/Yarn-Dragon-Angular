import { Component, OnInit } from '@angular/core';
import { ICart } from 'src/app/interfaces/cart';
import { CartService } from 'src/app/services/cart/cart.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {

  cartItems: ICart[] = [];
  isLoading = false;

  constructor(private cartService: CartService, private router: Router, private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    this.cartItems = this.cartService.getCart();
    console.log(this.cartItems);
  }

  checkout() {
    this.isLoading = true;
    this.cartService.checkout().subscribe(results => {
      this.cartService.clearCart();
      this.router.navigate(['products']);
      this.snackbar.open('Checkout successful!', 'Ok', { duration: 3000, verticalPosition: 'top'});
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
      this.snackbar.open('Checkout Failed', 'Ok', { verticalPosition: 'top'});
    });
  }

  getCartTotal() {
    let cartTotal = 0;
    this.cartItems.forEach(item => {
      cartTotal += item.price * item.quantity;
    });

    return cartTotal;
  }

  quantityUpdated(item) {
    this.cartService.updateCartQuantity(item, item.quantity);
  }

  removeCartItem(item) {
    this.cartService.deleteFromCart(item);
    this.cartItems = this.cartService.getCart();
  }

  clearCart() {
    this.cartService.clearCart();
    this.cartItems = [];
  }

}
