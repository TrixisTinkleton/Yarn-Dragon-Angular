import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ILoginRequest } from 'src/app/interfaces/login-request';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  isRegister = false;
  form: FormGroup;
  hasError = false;
  errorMessage: string;
  isLoading = false;

  constructor(private authService: AuthService, private router: Router, private snackbar: MatSnackBar, private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required],
      confirmPassword: ['']
    });
   }

  ngOnInit(): void {
  }

  loginClicked() {
    if (this.isRegister) { return; }
    this.isLoading = true;
    const loginRequest: ILoginRequest = {
      Email: this.form.get('email')?.value,
      Password: this.form.get('password')?.value
    };

    this.authService.login(loginRequest).subscribe((jwtResult: any) => {
      this.authService.jwt = jwtResult.jwt;
      this.router.navigate(['products']);
      this.isLoading = false;
    }, (error) => {
      this.isLoading = false;
      if (error.status == 401) {
        this.snackbar.open('Email address or password is incorrect', 'Ok', { verticalPosition: 'top'});
        return;
      }
      this.snackbar.open('Something went wrong', 'Ok', { verticalPosition: 'top'});
    });
  }

  registerClicked() {
    if (this.isRegister) {
      if (this.form.get('password')?.value != this.form.get('confirmPassword')?.value) {
        this.hasError = true;
        this.errorMessage = 'Passwords do not match';
        return;
      }
      this.isLoading = true;

      const registerRequest: ILoginRequest = {
        Email: this.form.get('email')?.value,
        Password: this.form.get('password')?.value
      };

      this.authService.register(registerRequest).subscribe((jwtResult: any) => {
        this.authService.jwt = jwtResult.jwt;
        this.router.navigate(['products']);
        this.snackbar.open('User created successfully!', 'Ok', { verticalPosition: 'top'});
        this.isLoading = false;
      }, (error) => {
        this.isLoading = false;
        if (error.status === 409) {
          this.snackbar.open('This email address has already been registered. Please use a different email address',
          'Ok', { verticalPosition: 'top'});
          return;
        }
        this.snackbar.open('Something went wrong', 'Ok', { verticalPosition: 'top'});
        return;
      });
    }

    this.isRegister = !this.isRegister;
  }

  registerBack() {
    this.clearForm();
    this.isRegister = false;
  }

  clearForm() {
    this.form.get('email').reset();
    this.form.get('password').reset();
    this.form.get('confirmPassword').reset();
  }
}
