import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ICart } from 'src/app/interfaces/cart';
import { IProductItem } from 'src/app/interfaces/product-item';
import { CartService } from 'src/app/services/cart/cart.service';
import { ProductService } from '../../services/products/product.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  products: IProductItem[];
  isLoading = false;
  constructor(private productService: ProductService, private cartService: CartService, private router: Router, private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    // this.mockProductData();
    this.getAllProducts();
  }

  getAllProducts() {
    this.isLoading = true;
    this.productService.getAllProducts().subscribe(products => {
      this.products = products;
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.snackbar.open('Failed to get products', 'Ok', { verticalPosition: 'top'});
    });
  }

  addItemToCart(item: IProductItem) {
    this.cartService.addToCart(item);
    this.snackbar.open('Item added to cart', null, { verticalPosition: 'top', duration: 3000});
  }



  mockProductData() {
    this.products = [
      {
        id: 1,
        name: 'Dragon scale dice bag',
        description: 'A bag made to look like a dragon egg and this is the long description so it needs more words but I don\'t have the energy to describe this fully',
        shortDesc: 'A bag made to look like a dragon egg',
        imageUrl: 'https://i.imgur.com/QXehKvk.jpg',
        price: 100,
        stock: 5
      },
      {
        id: 1,
        name: 'Dragon scale dice bag',
        description: 'A bag made to look like a dragon egg and this is the long description so it needs more words but I don\'t have the energy to describe this fully',
        shortDesc: 'A bag made to look like a dragon egg',
        imageUrl: 'https://i.imgur.com/QXehKvk.jpg',
        price: 100,
        stock: 5
      },
      {
        id: 1,
        name: 'Dragon scale dice bag',
        description: 'A bag made to look like a dragon egg and this is the long description so it needs more words but I don\'t have the energy to describe this fully',
        shortDesc: 'A bag made to look like a dragon egg',
        imageUrl: 'https://i.imgur.com/QXehKvk.jpg',
        price: 100,
        stock: 5
      },
      {
        id: 1,
        name: 'Dragon scale dice bag',
        description: 'A bag made to look like a dragon egg and this is the long description so it needs more words but I don\'t have the energy to describe this fully',
        shortDesc: 'A bag made to look like a dragon egg',
        imageUrl: 'https://i.imgur.com/QXehKvk.jpg',
        price: 100,
        stock: 5
      },
      {
        id: 1,
        name: 'Dragon scale dice bag',
        description: 'A bag made to look like a dragon egg and this is the long description so it needs more words but I don\'t have the energy to describe this fully',
        shortDesc: 'A bag made to look like a dragon egg',
        imageUrl: 'https://i.imgur.com/QXehKvk.jpg',
        price: 100,
        stock: 5
      },
      {
        id: 1,
        name: 'Dragon scale dice bag',
        description: 'A bag made to look like a dragon egg and this is the long description so it needs more words but I don\'t have the energy to describe this fully',
        shortDesc: 'A bag made to look like a dragon egg',
        imageUrl: 'https://i.imgur.com/QXehKvk.jpg',
        price: 100,
        stock: 5
      },
      {
        id: 1,
        name: 'Dragon scale dice bag',
        description: 'A bag made to look like a dragon egg and this is the long description so it needs more words but I don\'t have the energy to describe this fully',
        shortDesc: 'A bag made to look like a dragon egg',
        imageUrl: 'https://i.imgur.com/QXehKvk.jpg',
        price: 100,
        stock: 5
      },
    ];
  }
}
